package RESTAssured;

import org.junit.jupiter.api.Test;

import static io.restassured.RestAssured.*;
import static org.hamcrest.Matchers.*;


public class Rest1 {

    @Test
    public void requestUsZipCode90210_checkPlaceNameInResponseBody_expectBeverlyHills() {
        given().
                when().
                get("http://zippopotam.us/us/90210").
                then().
                assertThat().
                body("places[0].'place name'", equalTo("Beverly Hills"));
    }
}
/* Request method:	GET
Request URI:	http://zippopotam.us/us/90210
Proxy:			<none>
Request params:	<none>
Query params:	<none>
Form params:	<none>
Path params:	<none>
Headers:		Accept=*
        Cookies:		<none>
Multiparts:		<none>
Body:			<none>
{
        "post code": "90210",
        "country": "United States",
        "country abbreviation": "US",
        "places": [
        {
        "place name": "Beverly Hills",
        "longitude": "-118.4065",
        "state": "California",
        "state abbreviation": "CA",
        "latitude": "34.0901"
        }
        ]
        }

        Process finished with exit code 0
         */