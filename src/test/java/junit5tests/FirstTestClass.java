package junit5tests;

import org.junit.jupiter.api.Test;

public class FirstTestClass {

    @Test
    public void firstMethod() {
        System.out.println("this is the first test method");
    }

    @Test
    public void secondMethod() {
        System.out.println("This is the second test method");
    }
}
